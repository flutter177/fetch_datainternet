import 'dart:convert';

import 'package:fetchdatainternet/detail_page.dart';
import 'package:flutter/material.dart';
import 'product.dart';
import 'package:http/http.dart' as http;

// ignore: must_be_immutable
class ProductsPage extends StatefulWidget {
  ProductsPage({Key? key}) : super(key: key);
  @override
  State<ProductsPage> createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  late Future<List<Product>> lists;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    lists = fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products Page'),
      ),
      body: Center(
        child: FutureBuilder<List<Product>>(
          future: lists,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return MyWidget(
                data: snapshot.data as List<Product>,
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  Future<List<Product>> fetchAlbum() async {
    final response =
        await http.get(Uri.parse('https://nodejs-simpleserver.herokuapp.com/'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productFromJson(response.body);
      //Product.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}

class MyWidget extends StatelessWidget {
  MyWidget({Key? key, required this.data}) : super(key: key);

  late List<Product> data;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: data.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => DetailProduct(product: data[index]))),
            child: ElementProduct(
              product: data[index],
            ),
          );
        });
  }
}

class ElementProduct extends StatelessWidget {
  ElementProduct({Key? key, required this.product}) : super(key: key);
  Product product;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(3),
      child: Row(
        children: [
          Image.asset("assets/images/${product.image}"),
          const SizedBox(
            width: 5,
          ),
          Expanded(
            child: Column(
              children: [
                Text(product.name),
                Text(product.description),
                Text('${product.price}'),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
