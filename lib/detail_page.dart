import 'package:flutter/material.dart';
import 'product.dart';

class DetailProduct extends StatelessWidget {
  const DetailProduct({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(product.name),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset("assets/images/${product.image}"),
            Text(product.name),
            Text(product.description),
            Text('$product.price'),
          ],
        ),
      ),
    );
  }
}
